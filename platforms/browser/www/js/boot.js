//Создал класс phaser с шириной равно ширине окна и высотой окна, 
//Phaser.CANVAS - тип в чем будет обрабатываться игра, '' - имя тега, в котором будет работать игра, по умолчанию body
var game = new Phaser.Game(window.innerWidth, window.innerHeight, Phaser.CANVAS, '');

//Добавляем состояния load и menu - названия состояний, а loadState и menuState - объекты состояний
game.state.add('load', loadState);
game.state.add('game', gameState);

//Запускаем состояние load
game.state.start('load');