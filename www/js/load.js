var loadState = {
	//preload и create - функции, которые автоматически запускаются при запуске состояния
	//preload - используем для загрузки ресурсов, например картинок и звуков(ассетов)
	preload: function(){
		//Надпись загрузки
		var loadingLabel = game.add.text(80, 150, 'loading...',{font:'30px Courier', fill:'#ffffff'});
		//Загружаем ассеты в объект game
		game.load.image('logo', 'assets/LogoBlack512.png');
		game.load.image('sky', 'assets/grid_bg.png');
		game.load.image('player', 'assets/paddleRed.png');
		game.load.image('ball', 'assets/ballBlue.png');
		game.load.image('button', 'assets/buttonDefault.png');

		game.load.image('blockBlue', 'assets/element_blue_rectangle.png');
		game.load.image('blockGreen', 'assets/element_green_rectangle.png');
		game.load.image('blockGrey', 'assets/element_grey_rectangle.png');
		game.load.image('blockPurple', 'assets/element_purple_rectangle.png');
		game.load.image('blockRed', 'assets/element_red_rectangle.png');
		game.load.image('blockYellow', 'assets/element_yellow_rectangle.png');


	},
	create: function(){
		//Системные настройки
		game.physics.startSystem(Phaser.Physics.ARCADE);
		game.scaleRatio = window.devicePixelRatio / 3;
		//Запускаем состояние меню
		game.state.start('game');

	}
}