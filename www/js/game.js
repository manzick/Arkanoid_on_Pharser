var gameState = {
	create: function(){
		//Площадка для шарика
		game.player = game.add.sprite(game.world.width/2, game.world.height*9/10, 'player');
		//Дикая формула от Паши, которая делает одинаковые пропорции на любом экране
		game.player.scale.setTo(game.world.width/4/game.player.width);
		//Координаты внутри объекта
		game.player.anchor.setTo(0.5);

		//Шарик
		game.ball = game.add.sprite(game.player.x, game.player.y - game.player.height - 10, 'ball');
		game.ball.scale.setTo(game.world.width/15/game.ball.width);
		game.ball.anchor.setTo(0.5);

		//Включение аркадной физики для шарика и площадки
		game.physics.arcade.enable([game.ball, game.player]);

		//Площадка не приобретает импульс от шарика 
		game.player.body.immovable = true;
		//Упругость шарика, для отскока
		game.ball.body.bounce.setTo(1);
		//Евент коллизии
		game.ball.body.collideWorldBounds = true;
		//game.ball.body.blocked = { none: false, up: true, down: false, left: true, right: true };

		//Скорость
		var V = 200;
		//Вектор начального направления для шарика
		//У = 0, сверху))
		game.ball.body.velocity.setTo(V, -V);

		//Имена цветов
		var colorName = ['blockBlue', 'blockGreen', 'blockGrey', 'blockPurple', 'blockRed', 'blockYellow'];
		//Массив врагов
		game.enemyArray = [];

		for (i = 0; i < 20; i++) {
			var rNumber = Math.floor(Math.random() * 6);
 			game.enemyArray[i] = game.add.sprite(0, 0, colorName[rNumber]);
			game.enemyArray[i].anchor.setTo(0.5);
			game.enemyArray[i].scale.setTo(game.world.width/7/game.enemyArray[i].width);
			//Коэфициент степенного ряда для x
			var u = i % 4 + 1;
			u = Math.pow((-1), u) * Math.ceil(u / 2);
			//Кастомный 0 для правой и левой части
			var customZero = (u / Math.abs(u)) * (-1) * (game.enemyArray[i].width * 3 / 4);
			game.enemyArray[i].x = customZero + (game.world.width / 2) + u * (game.enemyArray[i].width * 6 / 4);
			//Коэфициент степенного ряда для y
			var w = Math.floor(i / 4) + 1;
			game.enemyArray[i].y = game.world.height*0.8/10 + w * (game.enemyArray[i].height * 7 / 4);
			game.physics.arcade.enable(game.enemyArray[i]);
			game.enemyArray[i].body.onCollide = new Phaser.Signal();
			game.enemyArray[i].body.onCollide.add(this.destroyYourself, this);
		}
		//Label с очками
		game.score = 0
		game.scoreLabel = game.add.text(game.world.width/2, game.world.height*0.7/10, game.score, {font: '50px Arial', fill: '#ffffff'});
		game.scoreLabel.anchor.setTo(0.5);
		//Сообщяет, жив ли персонаж
		game.life = true;
	},
	update: function(){
		//Функция, которая выполняется каждый тик
		//Обрабатывает сами столкновения
		game.physics.arcade.collide(game.player, game.ball);
		for (i = 0; i < 20; i++) {
			game.physics.arcade.collide(game.ball, game.enemyArray[i]);
		}
		//Следит за касаниями
		game.player.x = game.input.pointer1.x;
		//Если мячик ниже игрока, смерть
		if (game.ball.y > game.player.y && game.life) {
			this.destroyEverything();
		}
	},
	destroyYourself: function(target) {
		//Функция уничтожает задетый блок, и дает скорость шарику
		var V = 200;
		game.ball.body.velocity.setTo(V, V);
		target.destroy();
		game.score += 1;
		game.scoreLabel.text = game.score;
		//Проверяем на выйгрыш, ведь в такой простой игре победить можно лишь набрав 20))
		if (game.score == 20) {
			this.youWin();
		}
	},
	destroyEverything: function() {
		//Функция, которая уничтожает все, если ты проиграл
		game.life = false;
		game.ball.destroy();
		game.player.destroy();
		for (i = 0; i < 20; i++) {
			game.enemyArray[i].destroy();
		}
		game.youLose = game.add.text(game.world.width/2, 0, 'Ты проиграл', {font: '50px Arial', fill: '#ffffff'});
		game.youLose.anchor.setTo(0.5);
		game.youLose.y = game.world.height/2 + game.scoreLabel.height*9/10;
		game.scoreLabel.y = game.world.height/2 - game.scoreLabel.height*9/10;
		console.log('Ты проиграл');
	},
	youWin: function() {
		//Функция которая уничтожает персонажа и мячик, ведь игрок победил
		game.ball.destroy();
		game.player.destroy();
		game.youWin = game.add.text(game.world.width/2, 0, 'Ты победил', {font: '50px Arial', fill: '#ffffff'});
		game.youWin.anchor.setTo(0.5);
		game.youWin.y = game.world.height/2 + game.scoreLabel.height*9/10;
		game.scoreLabel.y = game.world.height/2 - game.scoreLabel.height*9/10;
		console.log('Ты победил');
	}
}